<?php

namespace Drupal\group_localized_config;

use Drupal\Core\Config\StorableConfigBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group_localized_config\Config\GroupLocalizedConfigHandler;

/**
 * Interface GroupLocalizedConfigPluginInterface.
 *
 * @package Drupal\group_localized_config
 */
interface GroupLocalizedConfigPluginInterface {

  /**
   * Populates a form with elements to configure the plugin.
   *
   * @param StorableConfigBase $config
   *   Config object.
   * @param string|null $language
   *   Langcode of the group currently being configured.
   *
   * @return array
   *   Form element.
   */
  public function add(GroupLocalizedConfigHandler $config, $module, $language);

  /**
   * Validate the results.
   *
   * @param StorableConfigBase $config
   *   Config object we will be saving our configuration to.
   * @param array $values
   *   An easier-to-read array of submitted values passed on by the main form.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string|null $language
   *   Langcode of the group currently being configured.
   */
  public function validate(GroupLocalizedConfigHandler $config, $module, array $values, array &$form, FormStateInterface $form_state, $language);

  /**
   * Submit the results.
   *
   * @param StorableConfigBase $config
   *   Config object we will be saving our configuration to.
   * @param array $values
   *   An easier-to-read array of submitted values passed on by the main form.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string|null $language
   *   Langcode of the locale currently being configured.
   * @return null
   *   Return nothing.
   */
  public function submit(GroupLocalizedConfigHandler $config, $module, array $values, array &$form, FormStateInterface $form_state, $language);

}
