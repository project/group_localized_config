<?php

namespace Drupal\group_localized_config\Annotation;
use Drupal\Component\Annotation\Plugin;

/**
 * Defines a GroupLocalizedConfigPlugin annotation object.
 *
 * @Annotation
 */
class GroupLocalizedConfigPlugin extends Plugin {

  /**
   * The plugin title.
   *
   * @var string
   */
  public $title;


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * An array of allowed roles.
   *
   * @var array
   */
  public $allowed_roles = [];


  /**
   * An array of allowed groups.
   *
   * @var array
   */
  public $allowed_groups = [];

  /**
   * An integer to determine the weight of the module.
   *
   * @var int
   */
  public $weight = NULL;

}
