<?php

namespace Drupal\group_localized_config\Plugin\GroupLocalizedConfig;

use Drupal\Core\Config\StorableConfigBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\group_localized_config\GroupLocalizedConfigPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\group_localized_config\Config\GroupLocalizedConfigHandler;

/**
 * An example Group Localized Config plugin.
 *
 * @GroupLocalizedConfigPlugin(
 *   title = "Example Plugin",
 *   id = "sample",
 *   weight = 0
 * )
 */
class GroupLocalizedConfigExamplePlugin extends PluginBase implements GroupLocalizedConfigPluginInterface, ContainerFactoryPluginInterface {

  /**
   * Constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function add(GroupLocalizedConfigHandler $config, $module, $language) {
    $form['sample_setting'] = [
      '#type' => 'textfield',
      '#title' => 'The setting',
      '#required' => TRUE,
      '#description' => 'This is a sample setting.',
      '#default_value' => $config->get($module, 'sample_setting', $language)
    ];
    $form['test_setting'] = [
      '#type' => 'textfield',
      '#title' => 'Test setting',
      '#required' => TRUE,
      '#description' => 'This is test setting.',
      '#default_value' => $config->get($module, 'test_setting', $language)
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(GroupLocalizedConfigHandler $config, $module, array $values, array &$form, FormStateInterface $form_state, $language) {
    if ($values['sample_setting'] == 'test') {
      $form_state->setError($form['module_sample']['sample_setting'], t('Sample Element: "test" is not allowed.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submit(GroupLocalizedConfigHandler $config, $module, array $values, array &$form, FormStateInterface $form_state, $language) {
    $config->set($module, 'sample_setting', $values['sample_setting'] ? $values['sample_setting'] : NULL, $language)
    ->set($module, 'test_setting', $values['test_setting'] ? $values['test_setting'] : NULL, $language);
    $config->save();
  }

}
