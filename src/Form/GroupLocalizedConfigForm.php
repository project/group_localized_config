<?php

namespace Drupal\group_localized_config\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Template\Attribute;
use Drupal\group_localized_config\GroupLocalizedConfigPluginManager;
use Drupal\Core\Config\ConfigManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\Html;
use Drupal\language\ConfigurableLanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\group_localized_config\Config\GroupLocalizedConfigHandler;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Class GroupLocalizedConfigForm.
 *
 * @package Drupal\group_localized_config\Form
 */
class GroupLocalizedConfigForm extends ConfigFormBase {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The Localized Config plugin manager.
   *
   * @var \Drupal\group_localized_config\GroupLocalizedConfigPluginManager
   */
  protected $GroupLocalizedConfigPluginManager;

  /**
   * The Drupal default config manager.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\TempStore\PrivateTempStoreFactory definition.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  private $tempStoreFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\ConfigManager $config_manager
   *   Config manager service.
   * @param \Drupal\group_localized_config\GroupLocalizedConfigPluginManager $group_localized_config_plugin_manager
   *   Localized Config plugin manager service.
   * @param \Drupal\language\ConfigurableLanguageManagerInterface $language_manager
   *   Language manager service.
   */
  public function __construct(ConfigFactory $config_factory, ConfigManager $config_manager, GroupLocalizedConfigPluginManager $group_localized_config_plugin_manager, MessengerInterface $messenger, PrivateTempStoreFactory $tempStoreFactory) {
    $this->configManager = $config_manager;
    $this->GroupLocalizedConfigPluginManager = $group_localized_config_plugin_manager;
    $this->messenger = $messenger;
    $this->tempStoreFactory = $tempStoreFactory;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'), $container->get('config.manager'), $container->get('plugin.manager.group_localized_config'), $container->get('messenger'), $container->get('tempstore.private'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'group_localized_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $language = NULL) {
    if($this->tempStoreFactory){
    $tempstore = $this->tempStoreFactory->get('common_library');
    $active_tab = $tempstore->get('centralized_config_active_tab');
    }
    $form['current_active_tab'] = [
      '#type' => 'hidden',
      '#attributes' => ['id' => 'current-active-tab'],
      '#default_value' => $active_tab ?? ''
    ];

    $first_language = \Drupal::service('gblc_library.common_service')->getFirstGroupLanguage();
    if (empty($first_language)) {
      $this->messenger->addError('No groups available.');
      return $form;
    }
    else {
      if (empty($language)) {
        $path = Url::fromRoute('group_localized_config.centralized_config', ['language' => $first_language])->toString();
        header('Location: ' . $path);
        exit;
      }
    }

    // Store the group`s language in the form state for future reference.
    $form_state->setStorage([
      'language' => $language,
    ]);

    // Draw the groups.
    $form['groups'] = $this->renderGroupsMenu($language);

    // Set up the container for the various settings.
    $form['modules'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['#attached'] = [
      'library' => [
          'group_localized_config/group_localized_config'
      ]
    ];

    // Fetch the user's roles to check against plugin definition permissions.
    $acct_roles = \Drupal::currentUser()->getRoles();

    // Fetch the registered config plugins from all modules...
    $plugin_definitions = $this->GroupLocalizedConfigPluginManager->getDefinitions();

    // ...sort them by weight...
    $comparison = function (array $a, array $b) {
      $weight_a = $a['weight'] ?? 0;
      $weight_b = $b['weight'] ?? 0;
      // Space ship operator, available since PHP 7.0.
      return $weight_a <=> $weight_b;
    };
    uasort($plugin_definitions, $comparison);
    // ...and iterate through them.
    foreach ($plugin_definitions as $definition) {
      // Only check roles IF plugin has allowed roles defined.
      if (!empty($definition['allowed_roles'])) {
        // Ensure the current user is permitted to see the config.
        if (!array_intersect($acct_roles, $definition['allowed_roles'])) {
          continue;
        }
      }

      // Allow form access as per defined groups.
      if (!empty($definition['allowed_groups'])) {
        if (!\Drupal::service('gblc_library.common_service')->checkIfAdmin()) {
          if (in_array($language, $definition['allowed_groups'])) {
            continue;
          }
        }
      }

      // Fetch the plugin's form elements.
      $plugin_form = $this->processPluginForm($definition, $language);
      if (!empty($plugin_form)) {
        $form['module_' . $definition['id']] = $plugin_form;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $storage = $form_state->getStorage();

    $language = $storage['language'];

    $trigger = $form_state->getTriggeringElement();

    if ($trigger && $trigger["#parents"][1] === 'submit') {
      $id = str_replace('module_', '', $trigger["#parents"][0]);

      $plugin_definitions = $this->GroupLocalizedConfigPluginManager->getDefinitions();
      if ($plugin_definitions[$id]) {
        $definition = $plugin_definitions[$id];
        // $plugin_config = $this->getSpecificConfig($definition['id'], $language);
        $plugin_values = $values['module_' . $definition['id']];

        $plugin = $this->GroupLocalizedConfigPluginManager->createInstance($definition['id']);
        $alp_config =  new GroupLocalizedConfigHandler();
        $plugin->validate($alp_config, $definition['id'], $plugin_values, $form, $form_state, $language);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $current_tab = $_REQUEST['modules__active_tab'];
    if($this->tempStoreFactory){
    $tempstore = $this->tempStoreFactory->get('common_library');
    $tempstore->set('centralized_config_active_tab', $current_tab);
    }

    $values = $form_state->getValues();
    $storage = $form_state->getStorage();

    $language = $storage['language'];

    $trigger = $form_state->getTriggeringElement();

    if ($trigger && $trigger["#parents"][1] === 'submit') {
      $id = str_replace('module_', '', $trigger["#parents"][0]);

      $plugin_definitions = $this->GroupLocalizedConfigPluginManager->getDefinitions();
      if ($plugin_definitions[$id]) {
        $definition = $plugin_definitions[$id];
        // $plugin_config = $this->getSpecificConfig($definition['id'], $language);
        $plugin_values = $values['module_' . $definition['id']];

        // Clean up the values.
        unset($plugin_values['enabled']);
        unset($plugin_values['submit']);

        $plugin = $this->GroupLocalizedConfigPluginManager->createInstance($definition['id']);
        $alp_config =  new GroupLocalizedConfigHandler();
        $plugin->submit($alp_config, $definition['id'], $plugin_values, $form, $form_state, $language);
      }
    }

    // Get the hidden field value for opened module.
    $open_module = $form_state->getValue('open_module', '');

    if ($open_module) {
      // Get current URL and add / replace the opened module.
      $url = Url::fromRoute('<current>');
      $query = $url->getOption('query');
      if (!is_array($query)) {
        $query = [];
      }
      $query['open_module'] = $open_module;
      $url->setOption('query', $query);

      // Set the form redirect to the new URL.
      $form_state->setRedirectUrl($url);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Fetches the form elements from a config plugin and prepares output.
   *
   * @param array|string $definition
   *   The plugin definition.
   * @param string|null|false $language
   *   A langcode representing the current language.
   *
   * @return array|false
   *   The resulting form elements.
   */
  protected function processPluginForm($definition, $language) {
    // Don`t display form in case no access of language.
    if (!\Drupal::service('gblc_library.common_service')->checkIfAdmin()) {
      if (!\Drupal::service('gblc_library.common_service')->userHasAccess($language)) {
        return;
      }
    }

    // If a plugin ID is provided, fetch the full definition.
    if (is_string($definition)) {
      $definition = $this->GroupLocalizedConfigPluginManager->getDefinition($definition);
    }
    $form = [];

    // Fetch the config for this particular locale.
    // $plugin_config = $this->getSpecificConfig($definition['id'], $language);

  // The main fieldset.
    $form = [
      '#type' => 'details',
      '#group' => 'modules',
      '#title' => $definition['title'],
      '#tree' => TRUE,
    ];

    $form['title'] = [
      '#markup' => '<h2 class="plugin-title">' . $definition['title'] . '</h2>',
      '#weight' => -200,
    ];

    $elements = [];
    try {
      // Create an instance of the plugin...
      $plugin = $this->GroupLocalizedConfigPluginManager->createInstance($definition['id']);

      // Fetch the plugin's form elements...
      $alp_config =  new GroupLocalizedConfigHandler();
      $elements = $plugin->add($alp_config, $definition['id'], $language);
    }
    catch (PluginException $exception) {
      // @todo Implement logger.
    }

    // ...then attach them to the overlying element!
    $form += $elements;

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save @title configuration', ['@title' => $definition['title']]),
      '#button_type' => 'primary',
      '#validate' => [
        [
          $this,
          'validateForm',
        ],
      ],
      '#submit' => [
        [
          $this,
          'submitForm',
        ],
      ],
    ];

    foreach (array_keys($elements) as $key) {
      $form['submit']['#limit_validation_errors'][] = [
        "module_{$definition['id']}",
        $key,
      ];
    }
    return $form;
  }

  /**
   * Get specified config.
   */
  protected function getSpecificConfig($id, $language) {
    $config = $this->configFactory()->getEditable('group_localized_config.' . $id);
    return $config;
  }

  /**
   * Display the tabs that can be used to switch between countries and locales.
   *
   * @param string|null $language
   *   The current language.
   *
   * @return array
   *   The rendered menu.
   */
  protected function renderGroupsMenu($language) {
    $languages = \Drupal::service('gblc_library.common_service')->getAllGroups();
    $items = [];
    foreach ($languages as $lang_code => $group_label) {
      if (\Drupal::service('gblc_library.common_service')->checkIfAdmin()) {
        $items[$lang_code] = [
          'title' => $group_label,
          // 'attributes' => new Attribute(),
          'is_expanded' => FALSE,
          'url' => Url::fromRoute('group_localized_config.centralized_config', [
            'language' => $lang_code,
          ]),
          'in_active_trail' => TRUE,
        ];
        if ($language == $lang_code) {
          $items[$lang_code]['attributes'] = new Attribute(['style' => 'background-color:yellow;']);
        }
      }
      else {
        if (\Drupal::service('gblc_library.common_service')->userHasAccess($lang_code)) {
          $items[$lang_code] = [
            'title' => $group_label,
            'attributes' => new Attribute(),
            'is_expanded' => FALSE,
            'url' => Url::fromRoute('group_localized_config.centralized_config', [
              'language' => $lang_code,
            ]),
            'in_active_trail' => TRUE,
          ];
          if ($language == $lang_code) {
            $items[$lang_code]['attributes'] = new Attribute(['style' => 'background-color:yellow;']);
          }
        }
      }

    }
    $menu = [
      '#theme' => 'menu',
      '#menu_name' => 'main',
      '#items' => $items
    ];
    return $menu;
  }

}
