<?php

namespace Drupal\group_localized_config;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Group Localized Config plugin manager.
 *
 * @see plugin_api
 */
class GroupLocalizedConfigPluginManager extends DefaultPluginManager {

  /**
   * Constructs a GeSettingsPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {

    // Will seek plugins in the GeSettings golder of the src folder.
    $subdir = 'Plugin/GroupLocalizedConfig';
    $interface = 'Drupal\group_localized_config\GroupLocalizedConfigPluginInterface';
    $annotation = 'Drupal\group_localized_config\Annotation\GroupLocalizedConfigPlugin';

    parent::__construct($subdir, $namespaces, $module_handler, $interface, $annotation);

    // This allows the plugin definitions to be altered by an alter hook.
    $this->alterInfo('group_localized_config_info');

    // This sets the caching method for our plugin definitions.
    $this->setCacheBackend($cache_backend, 'group_localized_config_info');
  }

}
