<?php
/**
 * @file
 * Getters and setters method.
 */
namespace Drupal\group_localized_config\Config;

use Drupal\Core\Config\Config;
class GroupLocalizedConfigHandler {

  public $config_details;

  /**
   * Get config based on group language.
   */
  public function get($module, $key, $language) {
    $key = $key . '-' . $language;
    $config = \Drupal::config('group_localized_config.' . $module);
    return $config->get($key);
  }

  /**
   * Set config based on group language.
   */
  public function set($module, $key, $value, $language) {
    $key = $key . '-' . $language;
    $config = \Drupal::configFactory()->getEditable('group_localized_config.' . $module);
    $config->set($key, $value);
    $config->save();
    $this->config_details = $config;
    return $this;
  }

  /**
   * Save configuration.
   */
  public function save() {
    $this->config_details->save();
    return $this;
  }
}
