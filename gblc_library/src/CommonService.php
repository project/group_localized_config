<?php

namespace Drupal\gblc_library;

use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;

/**
 * Class CommonService
 * @package Drupal\gblc_library
 */
class CommonService {

  protected $currentUser;

  /**
   * CustomService constructor.
   * @param AccountInterface $currentUser
   */
  public function __construct(AccountInterface $currentUser) {
    $this->currentUser = $currentUser;
  }

  /**
   * Check if user is admin.
   */
  public function CheckIfAdmin() {
    $current_user = $this->currentUser;
    $account = User::load($current_user->id());
    $roles = $current_user->getRoles();
    // All permissions to admin role.
    if (in_array('administrator', $roles)) {
      return TRUE;
    }
    return FALSE;
  }

 /**
  * Check if user has access to group.
  */
  public function userHasAccess($lang_code) {
    $current_user = $this->currentUser;
    $account = User::load($current_user->id());
    $roles = $current_user->getRoles();

    // All permissions to admin role.
    if (in_array('administrator', $roles)) {
      return TRUE;
    }

    // Get groups of logged in user.
    $groups = \Drupal::service('group.membership_loader')->loadByUser($account);
    $languages = [];
    foreach ($groups as $group_membership) {
      $group_obj = $group_membership->getGroup();
      $languages[] = $group_obj->label->value;
    }
    if (!empty($languages)) {
      $count = 0;
      if (in_array($lang_code, $languages)) {
        $count++;
      }
      if ($count > 0) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Get all groups.
   */
  public function getAllGroups() {
    $database = \Drupal::database();
    $countries_query = $database->select('groups_field_data', 'gfd')
    ->fields('gfd', ['langcode', 'label']);
    //->condition('gfd.type', 'country');
    $countries = $countries_query->execute()->fetchAll();
    $countries_arr = [];
    if (count($countries) > 0) {
      foreach ($countries as $key => $value) {
        $countries_arr[$value->label] = $value->label;
      }
    }
    return $countries_arr;
  }

  /**
   * Get logged in user first group`s language.
   */
  public function getFirstGroupLanguage() {
    $languages = $this->getAllGroups();
    $all_lang_codes = [];
    foreach ($languages as $lang_code => $group_label) {
      if ($this->checkIfAdmin()) {
        $all_lang_codes[] = $lang_code;
      }
      else {
        if ($this->userHasAccess($lang_code)) {
          $all_lang_codes[] = $lang_code;
        }
      }
    }

    $first_group = '';
    if (count($all_lang_codes) > 0) {
      $first_group = $all_lang_codes[0];
    }
    return $first_group;
  }

  /**
   * Sanitize input.
   */
  public function sanitizeInput($string) {
    $string = trim($string);
    if (empty($string)) {
      return $string;
    }
    $string = strip_tags($string);
    $string = htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
    $string = \Drupal\Component\Utility\Html::escape($string);
    $string = \Drupal\Component\Utility\Xss::filter($string);
    return $string;
  }

  /**
   * Get Taxonomy Terms.
   *
   * @param string $vid
   *   A string of Taxonomy Name.
   *
   * @return array
   *   An array of Options.
   */
  public function fetchTaxonomyTerms($vid) {
    $query = \Drupal::database()->select('taxonomy_term_field_data', 'tfd');
    $query->fields('tfd', ['tid', 'name']);
    $query->condition('tfd.vid', $vid);
    $terms = $query->execute()->fetchAllKeyed(0, 1);
    return $terms;
  }

  /**
   * Partial string match in array.
   */
  public function partialSearchInArray($arr, $keyword) {
    $all_indexes = [];
    foreach($arr as $index => $string) {
      if (preg_match("#^$keyword#i", $string) === 1) {
        $all_indexes[] = $index;
      }
    }
    return $all_indexes;
  }

  /**
   * Remove duplicate items from associative array.
   */
  public function removeDuplicatesFromArray($arr, $given_key) {
    $process_arr = [];
    $unique = [];
    foreach ($arr as $key) {
      if (!isset($process_arr[$key[$given_key]])) {
        $unique[] = $key;
        $process_arr[$key[$given_key]] = 1;
      }
    }
    return $unique;
  }

  /**
   * Get config variable value.
   */
  public static function getConfigSettings($service_name, $variable) {
    $config = \Drupal::config($service_name);
    $value = $config->get($variable);
    return $value;
  }

}
