Please make sure this module has been downloaded using composer.

How to install this module?
---------------------------
1. Install this module by drush command
- drush en group_localized_config
- drush en gblc_library
2. Or install by "Extend" menu. (Drupal Admin Backend)

How to use this module?
-----------------------
Please navigate to drupal site and follow config path.
http://sitename/admin/group/centralized_config
