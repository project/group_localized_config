jQuery('document').ready(function () {
  var active_tab = jQuery('#current-active-tab').val();
  if (active_tab) {
    jQuery('.vertical-tabs__menu-item').removeClass('is-selected');
    jQuery(".vertical-tabs__menu-item a").each(function () {
      if (jQuery(this).attr('href') == '#' + active_tab) { jQuery(this).parent('li').addClass('is-selected'); }
    });
    jQuery('.vertical-tabs__pane').hide();
    jQuery('#' + active_tab).show();
    jQuery('.vertical-tabs__active-tab').val(active_tab);
  }
});
